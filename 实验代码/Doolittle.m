function [x,y,l,u] = Doolittle(A,d)
    n=size(A,1);%获得矩阵A的行数
    n1=size(A,2);
    m=size(d,1);
    for i=1:m
        b(i)=d(i,1);
    end
    %这里b是一个数组
    u=zeros(n);%生成n*n的全零矩阵
    a=zeros(n);
    for i=1:n
        for j=1:n1
            a(i,j)=A(i,j);
        end
    end
    l=zeros(n);
    for i=1:n
        l(i,i)=1;
    end   
    for k=1:n
        for j=k:n
            sum1=0;
            for r=1:k-1
                sum1=sum1+l(k,r)*u(r,j);
            end
            u(k,j)=a(k,j)-sum1;
        end
        for i=k+1:n
            sum2=0;
            for r=1:k-1
                sum2=sum2+l(i,r)*u(r,k);
            end
            l(i,k)=(a(i,k)-sum2)/u(k,k);
        end
    end
    l
    u
    for i=1:n
        sum3=0;
        for j=1:(i-1)
            sum3=sum3+l(i,j)*y(j);
        end
        y(i)=b(i)-sum3;
    end
    for i=n:-1:1%注意matlab循环变量的步进是要注明正负的
        sum4=0;
        for j=(i+1):n
            sum4=sum4+u(i,j)*x(j);
        end
        x(i)=(y(i)-sum4)/u(i,i);
    end
end