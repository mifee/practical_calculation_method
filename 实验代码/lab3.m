%% 实用计算方法实验三——龙格库塔法解一阶常微分方程
% 姓名：林国瑞
% 
% 学号：9151010F0118

%已知一阶导数 龙格库塔法
%遇到角度注意是弧度制还是角度制，matlab中三角函数采用弧度制
clear
clc
x0 = 0;
xn = 10;
f = inline('cos(x)' , 'x' , 'y') ;
h = 0.2;
y0= 0;
x = x0:h:xn ;  % ==  计算节点 ==
y(1) = y0 ;
N = length(x) ;  % ==  节点数 ==
% 参数初始化，以下是通用参数 ===课本（1）
c = [1/6 1/3 1/3 1/6] ;
r = [0.5 0.5 1] ;
u = [0.5 0 0.5 0 0 1] ;

for k = 1:N-1  % ==  以下就直接按书本公式写的 ==
    kk(1) = f( x(k) , y(k) ) ;
    kk(2) = f( x(k)+r(1)*h , y(k) + u(1)*h*kk(1) ) ;
    kk(3) = f( x(k)+r(2)*h , y(k) + u(2)*h*kk(1) + u(3)*h*kk(2) ) ;
    kk(4)  = f( x(k)+r(3)*h  , y(k)  + u(4)*h*kk(1)  + u(5)*h*kk(2)  + u(6)*h*kk(3) ) ;
    y(k+1)  = y(k)  + h*  ( c(1)*kk(1)  + c(2)*kk(2)  +  c(3)*kk(3)  + c(4)*kk(4) ) ;
end
%if (0 == nargout)
for k = 1:N
    y2(k)=sin(x(k));%y2为解析解,y为数值解
    fprintf( 'x: %f \t y: %f  y2: %f w :%f\n' , x(k) , y(k), y2(k), y(k)-(y2(k)) ) ;
end
plot(x,y)
grid on